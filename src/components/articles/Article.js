import React, { Component } from 'react'
import { FlatList, Text, View, StyleSheet, TouchableOpacity, Image, Dimensions, SafeAreaView, ActivityIndicator } from 'react-native'
import {connect} from 'react-redux'
import {fetchArticle, deleteArticle} from './../../redux/actions/articleActions'

class Article extends Component {
    constructor(props){
        super(props)
        this.state = {
            page: 1,
            isLoading: false
        }
    }
    componentDidMount(){
        this.props.fetchArticle(this.state.page)
    }
    handleLoadMore = () => {
        this.state.page >= this.props.totalPages ? 
        this.setState({isLoading: false}) :
        this.setState({isLoading: true, page: this.state.page + 1})
        this.props.fetchArticle(this.state.page)
    }
    renderFooter = () => {
        return (
            this.state.isLoading && <ActivityIndicator size="large" /> 
        )
    }
    render() {
        console.log(this.props.articles)
        const {articles, deleteArticle} = this.props
        return (
            <SafeAreaView>
                <FlatList 
                    data={articles}
                    renderItem={({item}) => 
                        <TouchableOpacity
                            onLongPress={() => deleteArticle(item.id)}
                        >
                            <Items 
                            imageSource={item.image_url}
                            title={item.title}
                            desc={item.description}
                            createdDate={item.created_date}
                            />
                        </TouchableOpacity>
                    }
                    keyExtractor={(item, index) => index.toString()}
                    onEndReached={this.handleLoadMore}
                    ListFooterComponent={this.renderFooter}

                />
            </SafeAreaView>
        )
    }
}


export function Items ({imageSource, title, desc, createdDate}){
    return (
        <View style={{flex: 1, flexDirection: 'row', backgroundColor: 'whitesmoke', margin: 5}}>
            <Image 
                style={{width: 110, height: 110}}
                source={{uri: imageSource }}
            />
           <View style={{flex: 1, flexDirection: 'column'}}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode="tail" 
                    style={{fontSize: 20, color: 'red', margin: 10}}>{title}</Text>
                <Text 
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{color: 'darkgray', margin: 10}}>{desc}</Text>
                <Text 
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{color: 'darkgray', margin: 10}}>{createdDate}</Text>
           </View>
            
        </View>
    )
}


// subscribe to store

const mapStateToProps = (state) => {
    return {
        articles: state.articleR.articles,
        totalPages: state.articleR.totalPages
    }
}

export default connect(mapStateToProps, {fetchArticle, deleteArticle})(Article)


