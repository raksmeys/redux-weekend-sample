export const actionType = {
    fetchArticle: 'GET_ARTICLE',
    deleteArticle: 'DELETE_ARTICLE'
}