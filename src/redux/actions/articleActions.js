import axios from 'axios'
import {baseURL, header} from './../../api/API'
import { actionType } from './actionTypes'

export const fetchArticle = (page) => {
    return async (dispatch) => {
        await axios(`${baseURL}articles?page=${page}&limit=15`, {
            method: 'GET',
            headers: header
        })
        .then(res => {
            console.log(res)
            dispatch({
                type: actionType.fetchArticle,
                payload: res.data.data,
                totalPages: res.data.pagination.total_pages
            })
        })
    }
}

export const deleteArticle = (id) => {
    return async (dispatch) => {
        await axios(`${baseURL}/articles/${id}`, {
            method: 'DELETE',
            headers: header
        })
        .then(res => {
            dispatch({
                type: actionType.deleteArticle,
                ID: id
            })
        })
    }
}