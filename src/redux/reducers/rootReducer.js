import {combineReducers} from 'redux'
import { articleReducer } from './articleReducer'

const reducer = {
    articleR: articleReducer
}

export const rootReducer = combineReducers(reducer)