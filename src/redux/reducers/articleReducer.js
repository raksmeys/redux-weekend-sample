import { actionType } from "../actions/actionTypes"


const initialState = {
    articles: [],
    totalPages: 1
}

export const articleReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.fetchArticle: 
            return {
                ... state,
                articles: state.articles.concat(action.payload),
                totalPages: action.totalPages
            }
        case actionType.deleteArticle:
            return {
                ... state,
                articles: state.articles.filter(article => article.id !== action.ID)
            }
        default:
            return state
    }
}