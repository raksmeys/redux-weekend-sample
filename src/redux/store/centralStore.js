import thunk from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux'
import { rootReducer } from '../reducers/rootReducer'

const middleWare = [thunk]

export const store = createStore(rootReducer, applyMiddleware(...middleWare))